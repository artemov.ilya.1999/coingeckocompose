package by.iartemov.coingeckocompose.domain.repository

import by.iartemov.coingeckocompose.domain.model.Coin
import kotlinx.coroutines.flow.Flow

interface CoinsRepository {

    suspend fun getCoins(): Flow<List<Coin>>
}