package by.iartemov.coingeckocompose.data.db

import by.iartemov.coingeckocompose.data.db.entity.CoinEntity
import by.iartemov.coingeckocompose.domain.model.Coin

fun CoinEntity.toModel() = Coin(
    id = id,
    symbol = symbol,
    name = name,
)

fun Coin.toDbEntity() = CoinEntity(
    id = id,
    symbol = symbol,
    name = name,
)

fun List<CoinEntity>.toModel() = map { it.toModel() }

fun List<Coin>.toDbEntity() = map { it.toDbEntity() }