package by.iartemov.coingeckocompose.data.network.api

import by.iartemov.coingeckocompose.data.network.api.responses.CoinResponse
import retrofit2.http.GET

interface CoinApiService {

    @GET("coins/list")
    suspend fun getCoins(): List<CoinResponse>
}
