package by.iartemov.coingeckocompose.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import by.iartemov.coingeckocompose.data.db.dao.CoinsDao
import by.iartemov.coingeckocompose.data.db.entity.CoinEntity

@Database(entities = [CoinEntity::class], version = 1)
abstract class CoinsDatabase : RoomDatabase() {

    abstract fun coinsDao(): CoinsDao

    companion object {

        const val DATABASE_NAME = "coins"
    }
}