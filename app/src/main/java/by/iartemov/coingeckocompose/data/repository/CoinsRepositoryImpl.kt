package by.iartemov.coingeckocompose.data.repository

import by.iartemov.coingeckocompose.data.db.dao.CoinsDao
import by.iartemov.coingeckocompose.data.db.toDbEntity
import by.iartemov.coingeckocompose.data.db.toModel
import by.iartemov.coingeckocompose.data.network.api.CoinApiService
import by.iartemov.coingeckocompose.data.network.toModel
import by.iartemov.coingeckocompose.domain.model.Coin
import by.iartemov.coingeckocompose.domain.repository.CoinsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class CoinsRepositoryImpl(
    private val apiService: CoinApiService,
    private val dao: CoinsDao,
) : CoinsRepository {

    override suspend fun getCoins(): Flow<List<Coin>> = withContext(Dispatchers.IO) {
        runCatching {
            apiService.getCoins().toModel()
        }.onSuccess {
            dao.insertCoins(it.toDbEntity())
        }
        dao.getCoins().map { it.toModel() }
    }
}