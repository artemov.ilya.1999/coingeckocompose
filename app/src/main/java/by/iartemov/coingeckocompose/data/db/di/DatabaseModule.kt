package by.iartemov.coingeckocompose.data.db.di

import androidx.room.Room
import by.iartemov.coingeckocompose.data.db.CoinsDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            CoinsDatabase::class.java,
            CoinsDatabase.DATABASE_NAME,
        ).build()
    }
    single { get<CoinsDatabase>().coinsDao() }
}