package by.iartemov.coingeckocompose.data.di

import by.iartemov.coingeckocompose.data.db.di.databaseModule
import by.iartemov.coingeckocompose.data.network.di.networkModule
import by.iartemov.coingeckocompose.data.repository.CoinsRepositoryImpl
import by.iartemov.coingeckocompose.domain.repository.CoinsRepository
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module

val dataModule = module {
    includes(networkModule, databaseModule)
    singleOf(::CoinsRepositoryImpl) bind CoinsRepository::class
}