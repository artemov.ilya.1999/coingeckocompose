package by.iartemov.coingeckocompose.data.network

import by.iartemov.coingeckocompose.data.network.api.responses.CoinResponse
import by.iartemov.coingeckocompose.domain.model.Coin

fun CoinResponse.toModel() = Coin(
    id = id,
    symbol = symbol,
    name = name,
)

fun List<CoinResponse>.toModel() = map { it.toModel() }