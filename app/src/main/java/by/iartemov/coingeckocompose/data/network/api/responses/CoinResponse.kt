package by.iartemov.coingeckocompose.data.network.api.responses

import com.google.gson.annotations.SerializedName

data class CoinResponse(
    @SerializedName("id") val id: String,
    @SerializedName("symbol") val symbol: String,
    @SerializedName("name") val name: String,
)
