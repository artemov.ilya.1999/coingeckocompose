package by.iartemov.coingeckocompose.data.network.di

import by.iartemov.coingeckocompose.BuildConfig
import by.iartemov.coingeckocompose.data.network.api.CoinApiService
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single<Converter.Factory> { GsonConverterFactory.create() }
    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .addConverterFactory(get())
            .build()
    }
    single { get<Retrofit>().create(CoinApiService::class.java) }
}