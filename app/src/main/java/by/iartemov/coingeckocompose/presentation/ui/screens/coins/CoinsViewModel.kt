package by.iartemov.coingeckocompose.presentation.ui.screens.coins

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.iartemov.coingeckocompose.domain.model.Coin
import by.iartemov.coingeckocompose.domain.repository.CoinsRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CoinsViewModel(private val repository: CoinsRepository) : ViewModel() {

    private val _state: MutableStateFlow<CoinState> = MutableStateFlow(CoinState.Loading)
    val state: StateFlow<CoinState>
        get() = _state.asStateFlow()

    var job: Job? = null

    init {
        getCoins()
    }

    fun getCoins() {
        job?.cancel()
        job = viewModelScope.launch {
            _state.emit(CoinState.Loading)
            repository.getCoins().collect { coins ->
                val newState = if (coins.isNotEmpty()) {
                    CoinState.ShowCoins(coins)
                } else CoinState.Error
                _state.emit(newState)
            }
        }
    }
}

sealed class CoinState {
    object Loading : CoinState()
    object Error : CoinState()
    data class ShowCoins(val coins: List<Coin>) : CoinState()
}