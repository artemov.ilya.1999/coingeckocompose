package by.iartemov.coingeckocompose.presentation.ui.screens.coins

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import by.iartemov.coingeckocompose.domain.model.Coin
import by.iartemov.coingeckocompose.presentation.ui.common.ErrorLayout
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun CoinsScreen(viewModel: CoinsViewModel = getViewModel()) {
    val state = viewModel.state.collectAsState().value
    Surface(modifier = Modifier.fillMaxSize()) {
        AnimatedContent(
            targetState = state,
            modifier = Modifier.fillMaxSize(),
        ) { state ->
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center,
            ) {
                when (state) {
                    CoinState.Loading -> {
                        CircularProgressIndicator(modifier = Modifier.size(80.dp))
                    }
                    CoinState.Error -> {
                        ErrorLayout(
                            onRetryClick = { viewModel.getCoins() },
                            modifier = Modifier.fillMaxSize(),
                        )
                    }
                    is CoinState.ShowCoins -> {
                        CoinsList(
                            coins = state.coins,
                            modifier = Modifier.fillMaxSize(),
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun CoinsList(
    coins: List<Coin>,
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState(),
) {
    LazyColumn(
        modifier = modifier,
        state = state,
    ) {
        items(
            items = coins,
            key = { coin -> coin.id },
        ) { coin ->
            CoinItem(
                coin = coin,
                modifier = Modifier
                    .fillParentMaxWidth()
                    .padding(
                        start = 4.dp,
                        end = 4.dp,
                        top = 8.dp,
                        bottom = 8.dp,
                    )
            )
            Divider()
        }
    }
}

@Composable
fun CoinItem(
    coin: Coin,
    modifier: Modifier = Modifier,
) {
    Column(modifier = modifier) {
        Text(
            text = remember(coin.symbol) { coin.symbol.uppercase() },
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            fontWeight = FontWeight.Bold,
        )
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
            Text(
                text = coin.name,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
        }
    }
}
