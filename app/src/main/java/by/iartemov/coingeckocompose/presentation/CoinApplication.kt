package by.iartemov.coingeckocompose.presentation

import android.app.Application
import by.iartemov.coingeckocompose.data.di.dataModule
import by.iartemov.coingeckocompose.presentation.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CoinApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CoinApplication)
            modules(dataModule, presentationModule)
        }
    }
}