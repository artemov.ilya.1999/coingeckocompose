package by.iartemov.coingeckocompose.presentation.di

import by.iartemov.coingeckocompose.presentation.ui.screens.coins.CoinsViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val presentationModule = module {
    viewModelOf(::CoinsViewModel)
}