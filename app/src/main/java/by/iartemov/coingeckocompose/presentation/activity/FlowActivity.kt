package by.iartemov.coingeckocompose.presentation.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import by.iartemov.coingeckocompose.presentation.ui.screens.CoinComposeApp
import by.iartemov.coingeckocompose.presentation.ui.screens.coins.CoinsScreen
import by.iartemov.coingeckocompose.presentation.ui.theme.CoinTheme

class FlowActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoinComposeApp()
        }
    }
}
